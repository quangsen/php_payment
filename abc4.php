<HTML>
<?php
//Sample PayPal Button Encryption: Copyright 2006-2010 StellarWebSolutions.com
//Not for resale  - license agreement at
//http://www.stellarwebsolutions.com/en/eula.php
//Updated: 2010 02 01

# private key file to use
$MY_KEY_FILE = __DIR__ . "/ssl/my-prvkey.pem";

# public certificate file to use
$MY_CERT_FILE = __DIR__ . "/ssl/my-pubcert.pem";

# Paypal's public certificate
$PAYPAL_CERT_FILE = __DIR__ . "/ssl/paypal_cert.pem";
// var_dump($MY_CERT_FILE);die();
# path to the openssl binary
$OPENSSL = "/usr/bin/openssl";

$form = array('cmd'  => '_hosted-payment',
    'cert_id' => '64JUWWQUNP84W',
    'subtotal'           => '50',
    'business'           => 'ZFSHJF7K8VACG',
    'billing_first_name' => 'Fulanito',
    'billing_last_name'  => 'Benganito',
    'billing_city'       => 'WAN CHAI',
    'billing_country'    => 'HK',
    'billing_address1'   => 'Kwok-kwongFlat 25, 12 / F,Acacia Building,150 Kennedy Road thoroughfare',
    'billing_state'      => 'WAN CHAI',
    'billing_zip'        => '999077',
    'paymentaction'      => 'sale',
    'return'             => 'http://paypal.dev/'

);

$encrypted = paypal_encrypt($form);
var_dump($encrypted);
function paypal_encrypt($hash)
{
    //Sample PayPal Button Encryption: Copyright 2006-2010 StellarWebSolutions.com
    //Not for resale - license agreement at
    //http://www.stellarwebsolutions.com/en/eula.php
    global $MY_KEY_FILE;
    global $MY_CERT_FILE;
    global $PAYPAL_CERT_FILE;
    global $OPENSSL;

    if (!file_exists($MY_KEY_FILE)) {
        echo "ERROR: MY_KEY_FILE $MY_KEY_FILE not found\n";
    }
    if (!file_exists($MY_CERT_FILE)) {
        echo "ERROR: MY_CERT_FILE $MY_CERT_FILE not found\n";
    }
    if (!file_exists($PAYPAL_CERT_FILE)) {
        echo "ERROR: PAYPAL_CERT_FILE $PAYPAL_CERT_FILE not found\n";
    }

    //Assign Build Notation for PayPal Support
    $hash['bn'] = 'StellarWebSolutions.PHP_EWP2';

    $data = "";
    foreach ($hash as $key => $value) {
        if ($value != "") {
            //echo "Adding to blob: $key=$value\n";
            $data .= "$key=$value\n";
        }
    }

    $openssl_cmd = "($OPENSSL smime -sign -signer $MY_CERT_FILE -inkey $MY_KEY_FILE " .
        "-outform der -nodetach -binary <<_EOF_\n$data\n_EOF_\n) | " .
        "$OPENSSL smime -encrypt -des3 -binary -outform pem $PAYPAL_CERT_FILE";

    exec($openssl_cmd, $output, $error);

    if (!$error) {
        return implode("\n", $output);
    } else {
        return "ERROR: encryption failed";
    }
};
?>

    <HEAD>
        <LINK REL=stylesheet HREF="/styles/stellar.css" TYPE="text/css">
        <TITLE>PHP Sample Donation using PayPal Encrypted Buttons</TITLE>
    </HEAD>

    <BODY bgcolor=white>
        <TABLE border=0>
            <TR>
                <TD align=center>
                    <h1>Sample Donation Page</h1>
                    <P>This page uses encrypted PayPal buttons for your security.</P>
                    <form action="https://securepayments.sandbox.paypal.com/webapps/HostedSoleSolutionApp/webflow/sparta/hostedSoleSolutionProcess" method="post">
                        <input type="hidden" name="cmd" value="_hosted-payment">
                        <!--input type="hidden" name="encrypted" value="<?php echo $encrypted; ?>"-->
                        <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----
MIIIdQYJKoZIhvcNAQcDoIIIZjCCCGICAQAxggE6MIIBNgIBADCBnjCBmDELMAkG
A1UEBhMCVVMxEzARBgNVBAgTCkNhbGlmb3JuaWExETAPBgNVBAcTCFNhbiBKb3Nl
MRUwEwYDVQQKEwxQYXlQYWwsIEluYy4xFjAUBgNVBAsUDXNhbmRib3hfY2VydHMx
FDASBgNVBAMUC3NhbmRib3hfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwu
Y29tAgEAMA0GCSqGSIb3DQEBAQUABIGAF5t09h4Bk2vq/S5EzmIVrB+7cRmpUV95
y6LFGCqJNRInyuoHKUPCoKJwUYl7l9i7Lk5vg5GA77oYmC6FcfsANGnmX7sfbKtR
Fth85DbMqBY6A5jPvKQRDCWVpSEOBEiHKImawVZ1BXPy7oXPpOj6d9jZ2nX8dc+e
NvJICx+dxGwwggcdBgkqhkiG9w0BBwEwFAYIKoZIhvcNAwcECFDHBywL4jrygIIG
+NnBCbx/QOAQ1ikhczz7lEEq8kNc7c/Gqbpg6CuncuJhEeSoJe91g48ZExgKbFb1
11Y00UJ4C4r0i+Rqzi2vuBx1lYD+A2lQsv4aEpEHwPCap5MmKtgmDU7bvAwszto2
RmtdqPLtshZv/MetecfQESgFjl4Pquq5ajBMLAn0UdFYfrhsEq2osmSNSt1/Gl5V
ztN7Ol1fy827n8I1WIPfjyX+AKaOm3gQopwIvtmRnuUqMODXVLuAf6h/Jccu1seG
yAn5ddb8RmCHJlkuu3r+L3aC+E6t57CLd2mo3Fp7EaueHFq+4UIbar+svnMtsfR+
k6NuEgNFpt2GvMn5AVj2jnhBlqHZqZeYsf/SareEkEzBXve15M6TfA6L98h98DN8
RBOHpkMqSVOegcVgfGauSNKV6WR7ai0ACQ7Mt7hOhB7FLSPNcdBMwFxiHHFqJsUI
5DzcmTyVaAVZS07vr2llFsuzi7xLei7ePDXhf4aqVSYP1OfJ9ZnVP0A1GeahSF3z
lv6ukwDPFQ97APpHIYePpvz50Atzmn53DV4TGXuAmddn/KGvjsf3oXmyZNWoRRi9
W1c5n0M8YJJ5+yN2fzpXfB5BQAGbeP8/elNlLB2Vt774tOtW9Gu6v5oQG21HW7/4
is3bUROkMCug9ceplGn9Z+LeQZTmTWa+mXXp5wolmvJdxjiHLM79TCJCrRWx0zfD
cbZa9C51kYkM9HhQrfidWlOxqhfS1mAsfheO7cqSyW/fAdkMEIeDyXXzDlcaaVKc
VpUA4h1Vy+QbxXO9oQ0tpIINFMj4T5Tykf6d+Vr1qBAurm2sqPQ4ry1Y8FnM0GtN
AB7k/XJ5KEfxAgw8j2UB0D4M+gUzBZvvYUG885/uRrS7vB6mXvvXMaTtVScm67K5
senpymF0iG+l7THJuLa8MQxhwx5T1MmG9S3r9bJ6x47Hg8QmxKp/lLAe7auBpqI0
xzt1TuQgnUHg3TCztjCH7eRjfOnZgIvnQs3J04YTkdiKbdsGK+Kkp+hxbkeS/2wN
lwH5BLJzuGE5d1YSxmssgsCoRdVd5nCxU10K+GsG08BPzUp+yW/oVAHUho9Xwtx/
HBqTcd/WfPQwP2fkK6Lwd4WjCSHo2uA9ES5h1J7vPh87Q2rB4ckwLDzQNej7yJGT
aSqdALcUMdLmEtfYe7DTWAyl6YBVoEpM3+q+0q7eSD/vbh92akUoBSH/4qeJK22l
X4n6Tpb6Qnfp3UrKW75guKjBGyn8UIiTg0t9nzhfbG478eZIoOO677vdly+gUKG0
FmCVXIMZYlQW+B52t14rVgyWHPbuHTY9HlRDqYEuthmUeQLS7/WHZblFzQmCI0m2
q+VgpiuBcl0iPGLU1C6OTiwmxT1CRhAUtR59QlcWnNQLdQWaGFFlirgzjT3KKVdC
t0ws5mx+CdgzVDpWAKL8TcbPtQRTHJsnK7K0b9B3UTGjAzdsSiA5kaLRlJEQ/UU/
l5Mh/6ZULiivPAvkZOr4OHNs6LUBDjufruD/Z1udiTsnJ/a0sefS91qIfa2k87sb
2X63EQWrLTrmP40uUNi6S+ZQxlB0HMeqxHhzoQ2glCBLUjia9mqfcCf9SKmKf2CH
Xlc1SPTa60qy32NvyIn+MEdL2LUKtgbA/m1//U2IlkDSmieMwn5rkosFEh23QlDl
YRb/YZNz6Bb50FQcuNy8zRa2aUpEjui8ZuiQav4yM4uZ78jYf3OHGY48kJKswmX/
KYU46q1RkL3j+ixi48PKlXkJxdlQl6Jzvr2gVeDPYbuYi1Vev0NZsSg0NoGSOGjN
jdOIX8YIs54s4wlsgGaH5xW/Cbyr/WiyYsOHLXDMklkr+VuDLYKvALyKdqO+XmPN
yJLLlZtKmV2gcrn/YcYW7XTPXWZO32CrWHO/Li/n5nPm1/egynYNZbbdTXSBLrUK
IWvY0gCaIHmglNEB0tq1c/LrinFQvbQ+w2a2H/dFI9R+w2H4uzAWTlrGaSZr9BcW
YGGtzrjzeFF2buCcMLCS0fD2UXbKVEJKjfNkxS6n/1QFPAZevAzCnDZi17vhj8h4
epQEClBwszVWlSfbDnJ+G9+A2eTFlMJCfa4juzTPBk7EhH732O3m3Pq4NzCqjLkg
eoBYAMdPsjaMi9Oj1fUv+wJCDLUIPUFd+MALcJ29wp7QbOtODc/T9kiD9gMPI/bs
IMjGHN4CU5Fc7S+wWzJ/VmdfwzokfRkH1RV8mcFwmTeAaxIkzVFdTv4ZQcxDRtwM
B2IwaLAJvK4Pz4Ts2PQMgx0dWV2olLt0So/4lruoPCYDGsBCITVSZs1zs70Ofeqn
6hP7XTV4u5xWQtnezRCLzqsZuQ8oGP/CfQeQUbWJPjs69tvbi5+pcVpjxd6zaWz8
eXdAozaqLz4M
-----END PKCS7-----">
                        <input type="submit" name="METHOD" value="Pay Now">
                    </form>
    </BODY>

</HTML>
