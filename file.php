<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>
    <style>
    .ul-wrapper {
        display: none;
    }
    </style>
    <script>
    $(document).ready(function() {
        $('.item-wrapper').click(function(event) {
            if ($(this).hasClass('active') == true) {
                $(this).removeClass('active');
                $('.ul-wrapper').css({
                    display: 'none',
                    transition: 'all 1s'
                });
            } else {
                $('.ul-wrapper').css({
                    display: 'none',
                    transition: 'all 1s'
                });
                $('.item-wrapper').removeClass('active');
                $(this).addClass('active');
                $('.active .ul-wrapper').css({
                    display: 'block',
                    transition: 'all 1s'
                });
            }
        });
    });
    </script>
</head>

<body>
    <div class="module-content">
        <ul class="accordion-menu">
            <li class="item-wrapper">
                <a href="#">Category 1</a>
            </li>
            <li>
                <div class="item-wrapper">
                    <a href="#">Category 2</a>
                    <ul class="ul-wrapper">
                        <li>
                            <a href="/category2">Sub 1 in category 2</a>
                        </li>
                        <li>
                            <a href="/category2">Sub 2 in category 2</a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="item-wrapper">
                    <a href="#">Category 3</a>
                    <ul class="ul-wrapper">
                        <li>
                            <a href="/category2">Sub 1 in category 3</a>
                        </li>
                        <li>
                            <a href="/category2">Sub 2 in category 3</a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</body>

</html>
