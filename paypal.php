<?php
session_start();
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
require __DIR__ . '/vendor/autoload.php';

$ClientID     = "Ab37WJTughFtYak0fS_ZRmrc_LBb1UV8q9taXWnD94C5bTgz37kq3eozGbB-dztQ09B3pM4tvDDx1JjC";
$ClientSecret = "ED4TRKSgS8Zr2kqX_lRFuCnTYU24B05--qbwjUlYgCCEoIr8pXfg8UJJ4lH0rwkxIGFr3Hg2CLVFnlTK";
$apiContext   = new ApiContext(
    new OAuthTokenCredential($ClientID, $ClientSecret)
);
if (isset($_POST['paynow'])) {
// ### CreditCard
    // A resource representing a credit card that can be
    // used to fund a payment.
    {
        $card = new CreditCard();
    }
    $type               = "visa";
    $credit_card_number = $_POST['credit_card_number'];
    $month              = $_POST['month'];
    $year               = $_POST['year'];
    $csc                = $_POST['csc'];
    $price              = $_SESSION["price"];
    $card->setType($type)
        ->setNumber($credit_card_number)
        ->setExpireMonth($month)
        ->setExpireYear($year)
        ->setCvv2($csc);
// ### FundingInstrument
    // A resource representing a Payer's funding instrument.
    // For direct credit card payments, set the CreditCard
    // field on this object.
    $fi = new FundingInstrument();
    $fi->setCreditCard($card);
// ### Payer
    // A resource representing a Payer that funds a payment
    // For direct credit card payments, set payment method
    // to 'credit_card' and add an array of funding instruments.
    $payer = new Payer();
    $payer->setPaymentMethod("credit_card")
        ->setFundingInstruments(array($fi));
// ### Amount
    // Lets you specify a payment amount.
    // You can also specify additional details
    // such as shipping, tax.
    $amount = new Amount();
    $amount->setCurrency("USD")
        ->setTotal($price);
// ### Transaction
    // A transaction defines the contract of a
    // payment - what is the payment for and who
    // is fulfilling it.
    $transaction = new Transaction();
    $transaction->setAmount($amount)
        ->setDescription("Payment description")
        ->setInvoiceNumber(uniqid());
// ### Payment
    // A Payment Resource; create one using
    // the above types and intent set to sale 'sale'
    $payment = new Payment();
    $payment->setIntent("sale")
        ->setPayer($payer)
        ->setTransactions(array($transaction));
// For Sample Purposes Only.
    $request = clone $payment;
// ### Create Payment
    // Create a payment by calling the payment->create() method
    // with a valid ApiContext (See bootstrap.php for more on `ApiContext`)
    // The return object contains the state.
    try {
        $payment->create($apiContext);
        echo "Check out complete";
    } catch (Exception $ex) {
        $error = $ex->getData();
        $error = json_decode($error);
        echo $error->name;
    }

}
