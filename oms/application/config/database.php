<?php


return array
(
    'default' => array
    (
        'type'       => 'MySQL',
        'connection' => array(
            'hostname'   => 'localhost',
            'username'   => 'root',
            'password'   => 'secret',
            'persistent' => FALSE,
            'database'   => 'oms',
        ),
        'table_prefix' => '',
        'charset'      => 'utf8',
        'time_zone'    => '+00:00',
        'profiling'    => TRUE,
    ),

);